const card0 = document.getElementById('img0'), card1 = document.getElementById('img1'), card2 = document.getElementById('img2');
const card3 = document.getElementById('img3'), card4 = document.getElementById('img4'), card5 = document.getElementById('img5');
const infoTxt = document.getElementById('info');

let revealedCards = ['./assets/img/card1.png', './assets/img/card2.png', './assets/img/card3.png', './assets/img/card1.png', 
'./assets/img/card2.png', './assets/img/card3.png'];
let cardV = [0, 0, 0, 0, 0, 0];
let turn = 0, score = 0;
let cardPlayed;
let secondCardPlayed;

function play(e) {
	let temp = event.target.id;
	temp = temp.slice(3, 4);
	if (turn == 0) {
		animateCss(`#${event.target.id}`, 'flip');
		cardV[temp] = 1;
		event.target.src = revealedCards[temp];
		cardPlayed = event.target;
		event.target.removeEventListener('click', play);
		turn++;
	} else if (turn == 1) {
		turn++;
		animateCss(`#${event.target.id}`, 'flip');
		secondCardPlayed = event.target;
		secondCardPlayed.src = revealedCards[temp];
		secondCardPlayed.removeEventListener('click', play);
		if (cardPlayed.src == secondCardPlayed.src) {
			turn = 0;
			cardV[temp] = 1;
			score++;
			infoTxt.innerHTML = `Score : ${score}`;
			if (score == 3) {
				infoTxt.innerHTML = 'Vous avez gagné !';
			}
		}
		setTimeout(function() {
			if (cardPlayed.src != secondCardPlayed.src) {
				turn = 0;
				animateCss(`#${cardPlayed.id}`, 'flip');
				animateCss(`#${secondCardPlayed.id}`, 'flip');
				secondCardPlayed.addEventListener('click', play);
				cardPlayed.addEventListener('click', play);
				secondCardPlayed.src = './assets/img/cardback.png';
				cardPlayed.src = './assets/img/cardback.png';
			}
		}, 600);

	}
}

function init() {
	revealedCards.sort(function() { return 0.5 - Math.random() });
}




function animateCss(element, animationName, callback) {
    const node = document.querySelector(element)
    node.classList.add('animated', animationName)

    function handleAnimationEnd() {
        node.classList.remove('animated', animationName)
        node.removeEventListener('animationend', handleAnimationEnd)

        if (typeof callback === 'function') callback()
    }

    node.addEventListener('animationend', handleAnimationEnd)
}


window.addEventListener('load', init);
card0.addEventListener('click', play);
card1.addEventListener('click', play);
card2.addEventListener('click', play);
card3.addEventListener('click', play);
card4.addEventListener('click', play);
card5.addEventListener('click', play);